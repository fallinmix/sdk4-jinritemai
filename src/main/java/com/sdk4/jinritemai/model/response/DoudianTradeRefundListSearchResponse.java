package com.sdk4.jinritemai.model.response;

import com.sdk4.jinritemai.DoudianPage;
import com.sdk4.jinritemai.DoudianResponse;
import com.sdk4.jinritemai.model.bean.DoudianRefundOrder;

public class DoudianTradeRefundListSearchResponse extends DoudianResponse<DoudianPage<DoudianRefundOrder>> {
}

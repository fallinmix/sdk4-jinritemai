package com.sdk4.jinritemai.model.request;

import com.sdk4.jinritemai.DoudianRequest;
import com.sdk4.jinritemai.model.response.DoudianOrderListResponse;
import com.sdk4.jinritemai.model.response.DoudianTradeRefundListSearchResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoudianTradeRefundListSearchRequest implements DoudianRequest<DoudianTradeRefundListSearchResponse> {
    private final String method = "trade.refundListSearch";

    /**
     * 子订单状态
     */
    private Long orderId;

    /**
     *  售后状态 1: 所有,
     * 2: PreSaleAllAudit,发货前退款待处理
     * 3: RefundAudit,发货后仅退款待处理
     * 4: ReturnAudit,退货待处理
     * 5: ExchangeAudit,换货待处理
     * 6: RefundFail,同意退款、
     * 退款失败 7: AuditRefund,同意退款、退款成功
     * 8: AfterSaleAudit,待商家处理
     * 9: ReturnReceive,待商家收货
     * 10: AuditRefunding,同意退款，退款中
     * 11: ArbitratePending,仲裁中
     * 12: Close,售后关闭
     * 13: ReturnShip,待买家退货
     * 14: WaitBuyerReceive,换货待用户收货
     * 15: ExchangeSuccess,换货成功
     * 16: Refuse,拒绝售后
     * 17: ReceiveRefuse,退货后拒绝退款
     * 18: UploadRefund,待商家上传退款凭证
     */
    private Integer status;

    /**
     *  0: TypeReturn, 退货   1: TypeRefund, 售后仅退款   2: TypePreSaleAll, 售前退款   3: TypeExchange, 换货  5: TypeAll, 所有
     */
    private Integer type;

    /**
     *  1: LogisticsStatusAll, 所有  2: Shipped, 已发货  3: NotShipped, 未发货
     */
    private Integer logisticsStatus;

    /**
     *  售后id
     */
    private Long aftersaleId;

    /**
     *  支付方式 1: PayTpeAll, 所有  2: Cod, 货到付款  3: Online, 在线支付
     */
    private Integer payType;

    /**
     *  仲裁状态 1: ArbitrationStatusAll, 所有  2: ArbitrationStatusNone, 无仲裁  3: Pending, 仲裁中  4: Finished,仲裁结束
     */
    private Integer arbitrateStatus;

    /**
     *  退款类型 0: Origin, 原路退回
     * 1: Offline, 线下退款
     * 2: Imprest,预付款退款
     * 3: Pledge,保证金退款
     * 4: None,无需退款
     * 5: RefundTypeAll, 所有
     */
    private Integer refundType;

    /**
     *  	物流单号
     */
    private Long logisticsCode;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间，必须大于等于开始时间
     */
    private String endTime;

    /**
     * 排序
     */
    private String orderBy;

    /**
     * 订单排序方式：0按时间升序， 1按时间降序 默认为1
     */
    private String isDesc;

    /**
     * 页数（默认为0，第一页从0开始）
     */
    private Integer page = 0;

    /**
     * 每页订单数（默认为10，最大100）
     */
    private Integer size = 10;

    @Override
    public Class<DoudianTradeRefundListSearchResponse> getResponseClass() {
        return DoudianTradeRefundListSearchResponse.class;
    }
}

package com.sdk4.jinritemai.model.bean;


import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * 售后订单明细
 * @author hq
 */
@Getter
@Setter
public class AftersaleItem {


    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品id
     */
    private Long productId;

    /**
     * 商品图片
     */
    private String productImg;

    /**
     * 商品数量
     */
    private Integer num;

    /**
     * 支付金额（单位分）
     */
    private Integer payAmount;

    /**
     * 邮费（单位分）
     */
    private Integer postAmount;

    /**
     * 退款金额（单位分）
     */
    private Integer refundAmount;

    /**
     * 退货运费 （单位分）
     */
    private Integer refundPostAmount;

    /**
     * 售后标签
     */
    private List<String> aftersaleService;

    /**
     * 商品规格
     */
    private List<Map<String, String>> skuSpec;

    /**
     * 退款类型（0 全额退款
     * 1 部分退款）
     */
    private Integer partType;
}

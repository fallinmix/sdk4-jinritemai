package com.sdk4.jinritemai.model.bean;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 售后订单信息
 */
@Getter
@Setter
public class DoudianRefundOrder {

    /**
     * 订单号
     */
    private Long orderId;

    /**
     * 售后单号
     */
    private Long aftersaleId;

    /**
     * 申请售后时间
     */
    private String applyTime;

    /**
     * 售后类型（0 售后退货退款
     * 1 售后退款
     * 2 售前退款
     * 3 换货）
     */
    private Integer aftersaleType;

    /**
     * 超时自动流转截止时间
     */
    private String statusDeadline;

    /**
     * 自动流转类型
     */
    private Integer deadlineType;

    /**
     * 售后流程描述（仅退款，退货退款等）
     */
    private String aftersaleProcessDesc;

    /**
     * 退货物流状态描述（未发货，已发货等）
     */
    private String aftersaleStatusDesc;

    /**
     * 售后原因描述
     */
    private String reasonDesc;

    /**
     * 退款类型（0 全额退款
     * 1 部分退款）
     */
    private Integer partType;

    /**
     * 店铺订单id（主订单id）
     */
    private Long pId;

    /**
     * 退款类型描述
     */
    private String aftersaleRefundTypeDesc;

    /**
     * 售后退款类型（0 原路退回
     * 1 货到付款线下退款
     * 2 备用金退款
     * 3 保证金退款
     * 4 无需退款）
     */
    private Integer refundType;

    /**
     * 退款状态（0 无需退款
     * 1 待退款
     * 2 退款中
     * 3 退款成功
     * 4 退款失败）
     */
    private Integer refundStatus;

    /**
     * 售后状态（0 售后初始
     * 6 售后申请
     * 27 拒绝售后申请
     * 12 售后成功
     * 28 售后失败
     * 7 售后退货中
     * 11 售后已发货
     * 29 售后退货拒绝
     * 13 售后换货商家发货
     * 14 售后换货用户收货）
     */
    private Integer aftersaleStatus;

    /**
     * 买家收件人名
     */
    private String postReceiver;

    /**
     * 仲裁状态（0 无仲裁记录
     * 1 仲裁中
     * 2 客服同意
     * 3 客服拒绝）
     */
    private Integer arbitrateStatus;

    /**
     * 剩余的催发货短信次数
     */
    private Integer urgeSmsCnt;

    /**
     * 备注
     */
    private String remark;

    /**
     *  明细
     */
    private List<AftersaleItem> aftersaleItems;
}
